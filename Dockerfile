FROM trion/ng-cli as base

WORKDIR /tmp

COPY . .

RUN npm install && ng build

FROM nginx

COPY --from=base /tmp/dist/question-web /usr/share/nginx/html



